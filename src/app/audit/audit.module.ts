import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AuditComponent } from './audit.component';
import { AuditRoutingModule } from './audit-routing.module';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule,AuditRoutingModule],
  declarations: [AuditComponent],
})
export class AuditModule {}
